package com.dong.library.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;


/**
 * 用来统计借阅排行榜
 */
@Data
public class Rank {

    private String id;
    private String name;
    private Integer brrCount;

}
