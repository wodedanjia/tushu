package com.dong.library.controller;

import com.dong.library.Service.AdminService;
import com.dong.library.entity.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/admins")
public class AdminController {

    @Autowired
    private AdminService adminService;



    /**
     * 获取所有管理员
     * @return
     */
    @GetMapping("/findAll")
    public List<Admin> list(){
        List<Admin> adminList=adminService.findAll();
        return adminList;
    }



    /**
     * 管理员登录
     * @param admin
     * @return
     */
    @PostMapping("/findById")
    public Admin findByAdmin(@RequestBody Admin admin){
        Admin admin1 =  adminService.findAdmin(admin);

        return admin1;
    }

    /**
     * 通过名字查找
     * @param name
     * @return
     */
    @GetMapping("/findByName/{name}")
    public Admin findByName(@PathVariable("name") String name ){
        Admin admin1 = adminService.findbyname(name);

        return admin1;
    }

    /**
     * 通过id查找
     * @param
     * @return
     */
    @GetMapping("/findById/{id}")
    public Admin findById(@PathVariable("id") int id ){
        Admin admin1 = adminService.findbyId(id);

        return admin1;
    }


    /**
     * 更新员工信息
     * @param admin
     * @return
     */
    @PutMapping("/update")
    public String update(@RequestBody Admin admin){
        String status = adminService.updateAdmin(admin);

        return status;
    }
}
