package com.dong.library.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dong.library.entity.Admin;
import org.springframework.stereotype.Repository;

@Repository
public interface Adminmapper extends BaseMapper<Admin> {
}
