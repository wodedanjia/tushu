package com.dong.library.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dong.library.entity.Brrow;
import org.springframework.stereotype.Repository;

@Repository
public interface Brrowmapper extends BaseMapper<Brrow> {
}
