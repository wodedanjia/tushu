package com.dong.library.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dong.library.entity.Sort;
import org.springframework.stereotype.Repository;

@Repository
public interface Sortmapper extends BaseMapper<Sort> {
}
